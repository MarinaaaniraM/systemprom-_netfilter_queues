.SUFFIXES: .cpp .o
OBJS = $(patsubst %.cpp,%.o,$(wildcard *.cpp))
LIBS = -lnetfilter_queue
CXXFLAGS += $(LIBS)
BIN = nf
DEBUG =


all	: $(BIN) clean
	chmod 4755 $<

$(BIN)	: $(OBJS)
	$(CXX) -o $(BIN) $< $(CXXFLAGS)


clean  :
	rm -f $(OBJS)
