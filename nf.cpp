#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <netinet/in.h>
#include <linux/types.h>
#include <linux/ip.h>
#include <linux/netfilter.h> /* Defines verdicts (NF_ACCEPT, etc) */
#include <libnetfilter_queue/libnetfilter_queue.h>

/* returns packet id */
static u_int32_t print_pkt (struct nfq_data *tb)
{
        int id = 0;
        struct nfqnl_msg_packet_hdr *ph;
        struct nfqnl_msg_packet_hw *hwph;
        u_int32_t mark,ifi; 
        int ret;
        unsigned char *data;

        ph = nfq_get_msg_packet_hdr(tb);
        if (ph) {
                id = ntohl(ph->packet_id);
                printf("hw_protocol=0x%04x hook=%u id=%u ",
                        ntohs(ph->hw_protocol), ph->hook, id);
        }

        hwph = nfq_get_packet_hw(tb);
        if (hwph) {
                int i, hlen = ntohs(hwph->hw_addrlen);

                printf("hw_src_addr=");
                for (i = 0; i < hlen-1; i++)
                        printf("%02x:", hwph->hw_addr[i]);
                printf("%02x ", hwph->hw_addr[hlen-1]);
        }

        mark = nfq_get_nfmark(tb);
        if (mark)
                printf("mark=%u ", mark);

        ifi = nfq_get_indev(tb);
        if (ifi)
                printf("indev=%u ", ifi);

        ifi = nfq_get_outdev(tb);
        if (ifi)
                printf("outdev=%u ", ifi);
        ifi = nfq_get_physindev(tb);
        if (ifi)
                printf("physindev=%u ", ifi);

        ifi = nfq_get_physoutdev(tb);
        if (ifi)
                printf("physoutdev=%u ", ifi);

/*        ret = nfq_get_payload(tb, (char**)&data);
        if (ret >= 0) //printf("payload_len=%d ", ret);
           for( int i=20; i<28 && i<ret; ++i)
             printf("%02X ", data[i]);
*/		
        fputc('\n', stdout);

        return id;
}
        


/* calcsum - used to calculate IP and ICMP header checksums using
 * one's compliment of the one's compliment sum of 16 bit words of the header
 */
unsigned short calcsum(unsigned short *buffer, int length)
{
	unsigned long sum; 	

	// initialize sum to zero and loop until length (in words) is 0 
	for (sum=0; length>1; length-=2) // sizeof() returns number of bytes, we're interested in number of words 
		sum += *buffer++;	// add 1 word of buffer to sum and proceed to the next 

	// we may have an extra byte 
	if (length==1)
		sum += (char)*buffer;

	sum = (sum >> 16) + (sum & 0xFFFF);  // add high 16 to low 16 
	sum += (sum >> 16);		     // add carry 
	return ~sum;
}

static int cb(struct nfq_q_handle *qh, struct nfgenmsg *nfmsg,
              struct nfq_data *nfa, void *data)
{
        struct iphdr *ip = NULL;
        int len = 0;
        u_int32_t id = 0;
        printf("entering callback\n");
        id = print_pkt(nfa);
        // Roman 30-12-2014
        len = nfq_get_payload ( nfa, (char**)&ip );
		
		// Roman if icmp and type=0 (reply) -> type=3 (unreachable)
		if( len > sizeof(struct iphdr) && ip->protocol == 1 && *((char*)(ip+1)) == 0) {
		  unsigned short seqnum = ntohs( *(((unsigned short*)(ip+1))+3));
		  char* newpkt = NULL;
		  *(((unsigned short*)(ip+1))+1) = 0; // null to crc place
		  //*((char*)(ip+1)) = 11; // new icmp type
		  //*(((char*)(ip+1))+1) = 0; // new subtype
	      //*(((unsigned short*)(ip+1))+3) = htons( seqnum+2); // increasing sequence number
		  
		  newpkt = new char[len+4];
		  memcpy( newpkt, (char*)ip, len);
		  ip = (struct iphdr*)newpkt; //!!! new packet buffer !!!
		  len += 4;
		  ip->tot_len = htons(len);
		  
		  // new icmp ctrlsum
		  //*(((unsigned short*)(ip+1))+1) = calcsum((unsigned short*)(ip+1),len-sizeof(struct iphdr));
		  *(((unsigned short*)(ip+1))+1) = calcsum((unsigned short*)(ip+1),len-sizeof(struct iphdr));
		  printf("Modifying ICMP (len=%d) ;)\n", len);
		  //return nfq_set_verdict(qh, id, /* Roman */ NF_ACCEPT,len, (unsigned char*)ip);
		  len = nfq_set_verdict(qh, id, /* Roman */ NF_ACCEPT,len, (unsigned char*)ip);
		  delete newpkt;
		  return len;
	    }
        
        return nfq_set_verdict(qh, id, NF_ACCEPT, 0, NULL);
}

int main(int argc, char **argv)
{
        struct nfq_handle *h;
        struct nfq_q_handle *qh;
        struct nfnl_handle *nh;
        int fd;
        int rv;
        char buf[4096] __attribute__ ((aligned));

        printf("opening library handle\n");
        h = nfq_open();
        if (!h) {
                fprintf(stderr, "error during nfq_open()\n");
                exit(1);
        }

        printf("unbinding existing nf_queue handler for AF_INET (if any)\n");
        if (nfq_unbind_pf(h, AF_INET) < 0) {
                fprintf(stderr, "error during nfq_unbind_pf()\n");

                printf ("errno: %s\n", strerror(errno));

                exit(1);
        }

        printf("binding nfnetlink_queue as nf_queue handler for AF_INET\n");
        if (nfq_bind_pf(h, AF_INET) < 0) {
                fprintf(stderr, "error during nfq_bind_pf()\n");
                exit(1);
        }

        printf("binding this socket to queue '0'\n");
        qh = nfq_create_queue(h,  0, &cb, NULL);
        if (!qh) {
                fprintf(stderr, "error during nfq_create_queue()\n");
                exit(1);
        }

        printf("setting copy_packet mode\n");
        if (nfq_set_mode(qh, NFQNL_COPY_PACKET, 0xffff) < 0) {
                fprintf(stderr, "can't set packet_copy mode\n");
                exit(1);
        }

        fd = nfq_fd(h);

        while ((rv = recv(fd, buf, sizeof(buf), 0)) && rv >= 0) {
                printf("pkt received\n");
                nfq_handle_packet(h, buf, rv);
        }

        printf("unbinding from queue 0\n");
        nfq_destroy_queue(qh);

#ifdef INSANE
        /* normally, applications SHOULD NOT issue this command, since
         * it detaches other programs/sockets from AF_INET, too ! */
        printf("unbinding from AF_INET\n");
        nfq_unbind_pf(h, AF_INET);
#endif

        printf("closing library handle\n");
        nfq_close(h);

        exit(0);
}
